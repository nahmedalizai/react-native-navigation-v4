import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
 
export default function Loader(props) {
  return (
    <View>
      <AnimatedLoader
        visible={props.isVisible}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("./loader.json")}
        animationStyle={styles.lottie}
        speed={1}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100
  },
});
