import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import {createAppContainer} from 'react-navigation'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {createDrawerNavigator} from 'react-navigation-drawer'
import { Transition } from 'react-native-reanimated';
import LoginScreen from '../screens/LoginScreen';
import DashboardScreen from '../screens/DashboardScreen';
import SettingsScreen from '../screens/SettingsScreen';

const DrawerNavigator = createDrawerNavigator({
    Home: {screen: DashboardScreen},
    Settings: {screen: SettingsScreen}
})


const MySwitch = createAnimatedSwitchNavigator(
  {
    LoginScreen : {screen: LoginScreen},
    DashboardScreen: {screen: DrawerNavigator}
  },
  {
    transition: (
      <Transition.Together>
        <Transition.Out
          type="slide-left"
          durationMs={500}
          interpolation="easeIn"
        />
        <Transition.In type="fade" durationMs={500} />
      </Transition.Together>
    ),
  }
);

const Menu = createAppContainer(MySwitch)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Menu;