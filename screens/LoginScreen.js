import React, {useEffect} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Loader from '../components/Loader';

export default function Login(props) {
  const [visible, setVisible] = React.useState(false)


  function loginButtonHandler() {
    setVisible(true);
    props.navigation.navigate('DashboardScreen');
    setVisible(false);
  }

  return (
    <View style={styles.container}>
      <View><Loader isVisible={visible}/></View>
      <View>
        <Button 
          title= "Login"
          onPress= {loginButtonHandler}
      />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DCDCDC',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {

  },
  button: {
    flexDirection: 'row',
    width:'100%',
    justifyContent: 'space-between',
    paddingHorizontal: 15
  },
});
