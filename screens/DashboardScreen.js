import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';

export default function Dashboard(props) {
  return (
    <View style={styles.container}>
        <Text>Dashboard</Text>
        <Button 
          title= "Logout"
          onPress= {()=> props.navigation.navigate('LoginScreen')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
